<?php
   include_once('/script-bin/dateselect.class.php');
   $myselects = new DateSelects();
?>


<!DOCTYPE html>
<html>
    <head>
      <meta charset='utf8' />
      <link rel='stylesheet' type='text/css' href='css/main.css' />
      <script type='text/javascript' src='script-bin/dateselect.js'></script>
    </head>
    <body>
        <div name='wrapper' id='wrapper'>
            <fieldset name='main' id='main'>
                <legend>Update Membership</legend>
                <form name='main-form' id='main-form' action='update.php' method='post'>
                    <label for='action'>What would you like to do?</label><br />
                    <select name='action' id='action'>
                      <option name='default' value='' disabled selected>Choose one...</option>  
                      <option name='freeze' id='freeze' value='freeze'>Freeze my membership</option>
                      <option name='cancel' id='cancel' value='cancel'>Cancel my membership</option>
                    </select><br />
                    <label for='first'>First name:</label><br />
                    <input type='text' name='first' id='first' /><br />
                    <label for='last'>Last name:</label><br />
                    <input type='text' name='last' id='last' /><br />
                    <label for='email'>E-mail:</label><br />
                    <input type='email' name='email' id='email' /><br />
                    <label for='dob'>Date of birth:</label><br />
                    <?php
                      $myselects->createMonthSelect(); 
                      $myselects->createDaySelect(); 
                      $myselects->createYearSelect();
                    ?>
                    <label for='details'>Please share your reasons for this change:</label><br />
                    <textarea name='details'></textarea>
                    <input type='submit' />
                </form>
            </fieldset>
        </div>
    </body>
</html>