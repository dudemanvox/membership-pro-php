<?php
class DateSelects
{
	private $months;
	private $curmonth;
	private $today;
	private $year;
	private $numDays;
 
	public function __construct()
	{
		$this->months = array(1=> 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$this->curmonth = date("n");
		$this->today = date("d");
		$this->year = date("Y");
		$this->numDays = 31;
	}
 
	public function createMonthSelect() 
	{
		//create the month select
		$html = "<select name=\"month\" id=\"month\">\n";
		for ($i =1; $i <= 12; $i++)
		{
			if ($i < 10) {
				$html .= "<option value=\"" . substr('0' . $i, 0, 2) . "\"";
				$html .= ($this->curmonth == $i) ? " selected=\"selected\">" : ">";
				$html .= $this->months[$i]."</option>\n";
			}
			else {
				$html .= "<option value=\"" . $i . "\"";
				$html .= ($this->curmonth == $i) ? " selected=\"selected\">" : ">";
				$html .= $this->months[$i]."</option>\n";
			}
		}
		$html .= "</select>\n";
		echo $html;
	}
 
	public function createDaySelect()
	{
		//create the day select
		$html = "<select name=\"day\" id=\"day\">\n";
		for ($i = 1; $i <= $this->numDays; $i++)
		{
			$html .= "<option value=\"$i\"";
			$html .= ($this->today == $i) ? " selected=\"selected\">" : ">";
			$html .= $i."</option>\n";
		}
		$html .= "</select>\n";
		echo $html;
	}
 
	public function createYearSelect()
	{
		//create the year select
		$html = "<select name=\"year\" id=\"year\">\n";
		for ($i = $this->year -100; $i <= $this->year; $i++)
		{
			$html .= "<option value=\"$i\"";
			$html .= ($this->year == $i) ? " selected=\"selected\">" : ">";
			$html .= $i."</option>\n";
		}
		$html .= "</select>\n";
		echo $html;
	}
}