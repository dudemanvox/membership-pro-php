window.onload = function() {
        changeDaySelect();
	document.getElementById('month').onchange = function(evt) {changeDaySelect()};
	document.getElementById('year').onchange = function(evt) {changeDaySelect()};
}
 
function daysInMonth(month,year) {
	return 32 - new Date(year,month, 32).getDate();
}
 
function changeDaySelect() {
	var selectedMonth = document.getElementById('month').value;
	var selectedYear = document.getElementById('year').value;
	var dayselect = document.getElementById('day');
	var selectedDay = dayselect.selectedIndex + 1;
	var numdays = daysInMonth((selectedMonth - 1),selectedYear);
	dayselect.options.length = 0;
	for (var i = 1; i <= numdays; i++) {
		if (i == selectedDay) {
			dayselect.options[dayselect.options.length]=new Option(('0'+i).slice(-2), ('0'+i).slice(-2), false, true);
		} else {
			dayselect.options[dayselect.options.length]=new Option(('0'+i).slice(-2), ('0'+i).slice(-2), false, false);
		}
	}
}