<?php
class member {
    private $action;
    private $first;
    private $last;
    private $email;
    private $dob;
    private $dob_string;
    private $dbh;
    private $db_to_use;
    
    public function __construct($postdata, $db) {
        // Create a member with the parameters submitted by form
        $this->action = $postdata['action'];
        $this->first = $postdata['first'];
        $this->last = $postdata['last'];
        $this->email = $postdata['email'];
        $this->dob = array('year'=>$postdata['year'],
                     'month'=>$postdata['month'],
                     'day'=>$postdata['day']
                     );
        // Use implode() to create an ISO date string
        $this->dob_string = implode('-', $this->dob);
        // Pass array of database objects into $dbh
        $this->dbh = $db;
    }
        
    public function showMember() {
        // Debug function to make sure the form data is being received and properly formatted
        echo "Action: " . $this->action . "\r\n"
        . "First: " . $this->first . "\n"
        . "Last: " . $this->last . "\n"
        . "E-mail: " . $this->email . "\n"
        . "DOB: " . $this->dob_string;
    }
    
    public function killConn() {
        $i = 0;
        $size = count($this->dbh);
        if (is_array($this->dbh)) {
            while ($i <= $size) {
                $this->dbh[$i] = null;
                $i++;
            }
        }
        else {
            $this->dbh = null;
        }
        echo $size . " connections closed successfully!<br>";
    }
    
    private function useThis($db_num) {
        $db_holder = $this->dbh[$db_num];
        $this->killConn();
        $this->dbh = $db_holder;
        echo count($this->dbh);
    }
    
    public function getStatus() {
        // Set $size of $db array for looping purposes
        $size = count($this->dbh);
        $i = 0;
        // Queries database and retrieves unique ID based on four parameters so as to be certain that the correct person is selected
        $query_params = array(
                            ':first' => $this->first,
                            ':last' => $this->last,
                            ':email' => $this->email,
                            ':dob_string' => $this->dob_string
                            );
        $sql = "SELECT customer_id, current_status FROM customers WHERE firstname = :first AND lastname = :last AND email = :email AND customer_type = 'member' AND bday = :dob_string";
        while ($i <= $size) {
            try {
                $stmt = $this->dbh[$i]->prepare($sql);
                $stmt->execute($query_params);
                $count = $stmt->rowCount();
            }
            catch(PDOException $e) {
                die('An error has occurred while retrieving member status: ' . $e->getMessage());
            }
            // If user is unique, get the ID number and status
            if ($count == 1) {
                $result = $stmt->fetchAll();
                $id = $result[0][0];
                $status = $result[0][1];
                //echo $id;
                //echo $status;
                echo "Found on DB " . $i . "<br>";
                $this->useThis($i);
                return array($id, $status);
            }
            elseif ($count == 0 && $i < $size) {
                $i++;
            }
            else {
                echo "No such member was found. Please check that the information entered is correct.";
                return 0;
            }
        }
    }
    
    private function bankerRound($num) {
        if (!strpos($num, '.')) { 
            return $num; 
        } 
        $num = '0' . $num; 
        $num_arr = explode('.', $num); 
        if (empty($num_arr[1]) || (strlen($num_arr[1]) < 3)) { 
            return $num; 
        } 
        $units = ltrim($num_arr[0], '0'); 
        $cents = substr($num_arr[1], 0, 2); 
        $extra = rtrim(substr($num_arr[1], 2), '0'); 
        $mils = substr($extra, 0, 1); 
        if (($mils < 5) || (($extra == 5) && !($cents % 2))) { 
            return $units . '.' . $cents; 
        } else { 
            return ltrim(bcadd(($units . '.' . $cents), '.01', 2), '0'); 
        }
    }
    
    private function getFamily($id_stat) {
        if (is_array($id_stat)) {
            $query_params = array(
                            ':responsible' => $id_stat[0]
                            );
            $sql = "SELECT customer_id, current_status FROM customers WHERE responsible_party_id = :responsible";
            try {
                $stmt = $this->dbh->prepare($sql);
                $stmt->execute($query_params);
                $count = $stmt->rowCount();
            }
            catch (PDOException $e) {
                die('An error has occurred while checking for family members: ' . $e->getMessage());
            }
            if ($count == 0) {
                return $count;
            }
            else {
                echo "If you cancel, your " . $count . " family member(s) will also be cancelled.<br>";
                $family = $stmt->fetchAll();
                return $family;
            }
        }
    }
    
    public function checkBalance() {
        
        if (is_array($id_stat = $this->getStatus())) {
            $query_params = array(
                            ':id' => $id_stat[0]
                            );
            $sql = "SELECT payment_id, amount FROM invoices WHERE customer_id = :id";
            // Retrieves all of member's payment IDs associated with invoices
            try {
                $stmt = $this->dbh->prepare($sql);
                $stmt->execute($query_params);
                $count = $stmt->rowCount();
                $invoices = $stmt->fetchAll();
                //echo "Invoice count: " . $count . "<br>";
                
            }
            catch(PDOException $e) {
                die('An error has occurred while retreiving invoices: ' . $e->getMessage());
            }
            $outstanding_bal = 0;
            foreach ($invoices as $row) {
                // Removes the non-textual keys from each row
                unset($row[0]);
                unset($row[1]);
                // Echos a text-grapic representation of the $row array for debugging
                //echo var_export($row) . "<br>";
                foreach ($row as $key=>$val) {
                    // Checks for unpaid invoices which have a payment_id of '0' and adds the amount to the outstanding balance
                    if ($key == 'payment_id' && $val == '0') {
                        //echo "Payment ID: " . $val . " -- ";
                        $outstanding_bal += $row['amount'];
                    }
                }
            }
            if ($outstanding_bal == 0) {
                echo "Your account has no outstanding balance.<br>";
                $this->getFamily($id_stat);
            }
            else {
                printf("You have an outstanding balance of: $%.2f. This must be settled before any changes can be made to the status of your account.<br>", $outstanding_bal);
                /** GENERATE INVOICE DETAILS AND ANY CC RESPONSE HERE **/
                /** TAKE PAYMENT ONLINE NOW ?? **/
                echo "Please contact Austin Rock Gym at (512)416-9299 and speak to a staff member about making this payment.<br>";
                echo "Once your payment has been made, return to the membership page to update your membership status.<br>";
            }
        }
        else {
            echo "No customer ID was provided to check balance.";
        }
    }
}