<?php

// Load database info and connect
require('common.php');

// Load member class
require_once('script-bin/member.class.php');

// Check if form was submitted and output matching customers
if (!empty($_POST['action'])) {
    $member = new member($_POST, $db);
    try {
        $var = $member->checkBalance();
        $member->killConn();
        
    }
    catch (PDOException $e) {
        die('An error has occured with member.class: ' . $e->getMessage());
    }
}
    
else {
    echo "Please complete the form to continue.";
    header('Location:membership.php');
    exit;
}