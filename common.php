<?php
   // Common connection credentials
   $user= ;
   $pass = ;
   $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
   // Unique credentials of first DB
   $host1 = ;
   $dbname1 = ;
   // Unique credentials of second DB
   $host2 = ;
   $dbname2 = ;
   
   // Connect to mySQL
   try {
      $db = array ( new PDO("mysql:host={$host1};dbname={$dbname1};charset=utf8", $user, $pass, $options),
                    new PDO("mysql:host={$host2};dbname={$dbname2};charset=utf8", $user, $pass, $options)
                  );
   }
   catch (PDOException $e) {
      die('An error has occured: ' . $e->getMessage());
   }
   
   // Set connection attributes.
   $i = 0;
   $size = count($db);
   while ($db <= $size ) {
      // Set error mode for each connection
      $db[$i]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $db[$i]->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $i++;
   }
   // Turn off magic quotes.
   if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) 
    { 
        function undo_magic_quotes_gpc(&$array) 
        { 
            foreach($array as &$value) 
            { 
                if(is_array($value)) 
                { 
                    undo_magic_quotes_gpc($value); 
                } 
                else 
                { 
                    $value = stripslashes($value); 
                } 
            } 
        } 
     
        undo_magic_quotes_gpc($_POST); 
        undo_magic_quotes_gpc($_GET); 
        undo_magic_quotes_gpc($_COOKIE); 
    }
    
    // Set browser to use UTF8.
    header('Content-Type: text/html; charset=utf-8');
    
    //Open user session.
    session_start();
    
    /************** DEBUG  **********/
    echo "Success!<br>";
    echo var_export($db) . "<br>";
    /********************************/