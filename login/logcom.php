<?php
   // Common connection credentials
   $user= 'angryfor_promem';
   $pass = 'b0mbad1l84tb!!!';
   $host = 'localhost';
   $dbname = 'angryfor_mempro';
   $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
   
   // Connect to mySQL
   try {
      $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $user, $pass, $options);
   }
   catch (PDOException $e) {
      die('An error has occured: ' . $e->getMessage());
   }
   // Set error mode for each connection
   $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
   // Turn off magic quotes.
   if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) 
    { 
        function undo_magic_quotes_gpc(&$array) 
        { 
            foreach($array as &$value) 
            { 
                if(is_array($value)) 
                { 
                    undo_magic_quotes_gpc($value); 
                } 
                else 
                { 
                    $value = stripslashes($value); 
                } 
            } 
        } 
     
        undo_magic_quotes_gpc($_POST); 
        undo_magic_quotes_gpc($_GET); 
        undo_magic_quotes_gpc($_COOKIE); 
    }
    
    // Set browser to use UTF8.
    header('Content-Type: text/html; charset=utf-8');
    
    //Open user session.
    session_start();